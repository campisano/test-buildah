FROM debian:bookworm-slim

RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/* /usr/share/man/*

RUN useradd -r -U app -d /srv/app -m
USER app:app

WORKDIR /srv/app
CMD ["echo", "This is a test"]
